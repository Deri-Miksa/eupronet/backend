import { Router } from "express";

import missingParams from "../middlewares/missingParams";
import handler from "../middlewares/handlerWrapper";
import auth from "../middlewares/auth";
import dispatch, { dispatchEnabled, setDispatchEnabled } from "../nats/dispatcher";
import { loggingEnabled, setLoggingEnabled } from "../nats/logger"
import { Order } from "../model/types";
import { queueOrder, FactoryState, assemblyLineState, checkColorReadings, checkRFIDColorPairs, checkActiveOrders, checkFinishedOrders } from "../model/handlers";
import { io } from "../app";

require('dotenv').config();

var router = Router();

router.post("/dispatch", auth("dispatcher"), async (req, res, next) => {
	var {
		subject,
		message
	} = req.body;

	if (missingParams(res, { subject, message })) return;

	var err = await dispatch(subject, message, true);
	if (err) return next(err)
	return res.send({ status: "ok" });
})

router.get('/credentials', auth("dispatcher"), async (req, res, next) => {

	return res.send({ status: "ok", username: process.env.NATS_user, password: process.env.NATS_password });

});


router.post('/active', auth("dispatcher"), async (req, res, next) => {
	var {
		enabled,
	} = req.body;

	if (missingParams(res, { enabled })) return;
	if (typeof enabled !== "boolean")
		return res.send({ status: "error", msg: "'enabled' should be type boolean!" })

	setDispatchEnabled(enabled);

	return res.send({ status: "ok" });
});

router.get('/active', auth("dispatcher"), async (req, res, next) => {
	return res.send({ status: "ok", dispatchEnabled });
});


router.post('/logging', auth("admin"), async (req, res, next) => {
	var {
		enabled,
	} = req.body;

	if (missingParams(res, { enabled })) return;

	setLoggingEnabled(enabled);

	return res.send({ status: "ok" });
});

router.get('/logging', auth("dispatcher"), async (req, res, next) => {
	return res.send({ status: "ok", loggingEnabled });
});

router.get('/messagelog', handler(async (req, res, next, conn) => {

	var limit = req.query.limit || req.body.limit || 10;
	var filter = req.query.subject || req.body.subject || "%";

	var messages = await conn.query(`
        SELECT 
			subject,
			code, payload,
            DATE_FORMAT(timestamp, "%Y-%m-%d %H:%i:%s") as timestamp
        FROM natslog
        WHERE subject LIKE ?
        ORDER BY timestamp DESC
        LIMIT ?`, [filter, limit]);

	//parse payload as JSON
	messages.forEach((e: any) => e.payload = JSON.parse(e?.payload));

	return res.send({ status: "ok", messages });

}));

router.post('/queueorder', auth("dispatcher"), handler(async (req, res, next, conn) => {
	var { id, orderedItems, assignedChips }: Order = req.body;
	if (missingParams(res, { id }, { id, orderedItems }, { id, orderedItems, assignedChips })) return;

	let order: Order | undefined;
	if (!orderedItems) {
		orderedItems = await conn.query(`
        SELECT colors.name as color, amount 
        FROM order_items INNER JOIN colors ON colors.id = order_items.color 
        WHERE \`order\` = ?`, [id]);

		if (orderedItems.length == 0)
			return res.send({ status: "error", msg: `There is no order with id '${id}'` })

		if(!assignedChips){
			assignedChips = await conn.query(`
			SELECT colors.name as color, rfid 
			FROM assigned_chips INNER JOIN colors ON colors.id = assigned_chips.color
			WHERE \`orderid\` = ?`, [id])
		}

		order = new Order(id, [...orderedItems], [...assignedChips]);
	}

	order = order ?? new Order(id, orderedItems, assignedChips ?? []);
	queueOrder(order);

	return res.send({ status: "ok" });
}));

router.post('/loadstate', auth("dispatcher"), handler(async (req, res, next, conn) => {
	let {
		colorReadings,
		RFIDColorPairs,
		activeOrders,
		finishedOrders
	}: FactoryState = req.body;

	let errors = [
		checkColorReadings(colorReadings),
		checkRFIDColorPairs(RFIDColorPairs),
		checkActiveOrders(activeOrders),
		checkFinishedOrders(finishedOrders)
	];
	if (errors.some(x => x != ""))
		return res.send({ status: "error", msg: `The following parameters are in a bad format: ${errors.join(", ")}` });

	if (colorReadings) assemblyLineState.colorReadings = colorReadings;
	if (RFIDColorPairs) assemblyLineState.RFIDColorPairs = RFIDColorPairs;
	if (activeOrders) assemblyLineState.activeOrders = activeOrders.map(o => Order.from(o));
	if (finishedOrders) assemblyLineState.finishedOrders = finishedOrders.map(o => Order.from(o))

	// console.log(assemblyLineState);
	io.emit("telemetry", assemblyLineState);

	return res.send({ status: "ok" })
}))


export default router;