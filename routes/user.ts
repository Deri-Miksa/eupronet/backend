import { Router } from "express";
import jwt from "jsonwebtoken";
import { SecurePass } from "argon2-pass";
import { v4 as uuidv4 } from "uuid";
import time from "strftime"

import missingParams from "../middlewares/missingParams";
import auth from "../middlewares/auth";
import validatePass from "../middlewares/validatePass";
import activity from "../middlewares/activitylogger";
import handler from "../middlewares/handlerWrapper";
import { assemblyLineState, queueOrder } from "../model/handlers";
import { Order, OrderItem } from "../model/types";
import { query } from "../bin/db";

require('dotenv').config();

var router = Router();
router.use(activity);

router.get('/healthcheck', function (req, res, next) {
    return res.send({
        status: 'ok'
    });
});

router.get('/colorfromrfid', auth("user"), handler(async (req, res, next, conn) => {
    var {
        rfid
    }: any = { ...req.query, ...req.body };

    if (missingParams(res, { rfid })) return;

	let color = assemblyLineState.RFIDColorPairs[rfid];
	if (!color) 
		return res.send({ status: "error", msg: `There are no chips associated with RFID: ${rfid}` });

    return res.send({ status: "ok", color });

}));

router.post('/insert/print', auth("user"), handler(async (req, res, next, conn) => {
    var {
        state,
        color,
        date
    } = req.body

    if (missingParams(res, { state, color })) return;

    // Get the country code
    var getCountryRes = await conn.query("SELECT country FROM users WHERE id = ?", [res.locals.userid]);
    var countryCode = getCountryRes[0].country;

    // Insert data
    if (!date)
        date = time("%Y-%m-%d %H:%M:%S");
    var insertRes = await conn.query("INSERT INTO queries (country, state, color, date) VALUES (?, ?, ?, ?)", [countryCode, state, color, date]);
    if (insertRes.insertId == 0)
        return res.send({ status: "error", msg: "Insertion failed!" });

    var prodStatsRes = await conn.query("CALL ProdStats()");

    // Send the change to all connected clients
    res.locals.io.emit("prodStats", prodStatsRes[0]);
    return res.send({ status: "ok" });

}));


router.post('/login', handler(async (req, res, next, conn) => {
    var {
        username,
        password
    } = req.body;
    if (missingParams(res, { username, password })) return;

    if (!password)
        return res.send({ status: "error", msg: "Password can't be empty!" });

    // Get the hashed password
    var hashDb = await conn.query("SELECT id, hash FROM users WHERE username = ?", [username]);
    if (hashDb.length == 0)
        return res.send({ status: "error", msg: "Authentication failed!" });
    res.locals.userid = hashDb[0].id;
    if (!validatePass(res, password, hashDb[0].hash)) return;

    // Generate a token
    var token = jwt.sign({ uid: res.locals.userid }, (process.env.JWTSecret || process.env.adminToken)!);

    return res.send({
        status: 'ok',
        token: token
    });
}));

router.post('/orders', auth("user"), handler(async (req, res, next, conn) => {
    var {
        orderItems
    } = req.body;

    if (missingParams(res, { orderItems })) return;

    if (!Array.isArray(orderItems))
        return res.send({ status: "error", msg: "'orderItems' is expected to be an array!" });

    let orderItemsUppercase: OrderItem[] = [];
    // Swap color names for ids
    for (const [index, { color, amount }] of orderItems.entries()) {
        if (!color || amount === undefined || amount < 0)
            return res.send({ status: "error", msg: "'orderItems' is expected to contain 'color' and a positive 'amount'" });

        var selectColor = await conn.query(`SELECT id FROM colors WHERE name LIKE ?`, [color]);
        if (selectColor.length === 0)
            return res.send({ status: "error", msg: `The given color '${color}' was not found in the database` });

        orderItems[index].color = selectColor[0].id;
        orderItemsUppercase.push({ color: color.toUpperCase(), amount })
    }

    // console.log(orderItemsUppercase)
    // Insert new order
    var orderID = uuidv4().split("-")[0];
    var date = time("%Y-%m-%d %H:%M:%S");
    var insertOrder = await conn.query(`INSERT INTO orders (id, user, state, date) VALUES (?, ?, ?, ?)`, [orderID, res.locals.userid, 1, date]);

    if (insertOrder.affectedRows === 0)
        return res.send({ status: "error", msg: "Falied to insert order into the database!" });

    // Insert order items
    for (const { color, amount } of orderItems) {
        if (amount == 0) continue;
        var insertItems = await conn.query(`INSERT INTO order_items (\`order\`, color, amount) VALUES (?, ?, ?)`, [orderID, color, amount]);
        if (insertItems.affectedRows === 0)
            return res.send({ status: "error", msg: "Failed to insert order items!" });
    }
	let order = new Order(orderID, orderItemsUppercase);
	res.locals.io.emit("new-order", order);
    queueOrder(order);

    return res.send({ status: "ok" });
}));

router.get('/orders', auth("user"), handler(async (req, res, next, conn) => {

    var orders = [...await conn.query(`SELECT id, date, state FROM orders WHERE user = ? ORDER BY date DESC`, [res.locals.userid])];

    orders = await Promise.all(orders.map(async (o) => await getOrderData(o.id)))

    return res.send({ status: "ok", orders });

}));

router.get('/allorders', auth("dispatcher"), handler(async (req, res, next, conn) => {

    var orders = [...await conn.query(`SELECT orders.id, username, date FROM orders INNER JOIN users ON users.id = user ORDER BY date DESC`)];

    orders = await Promise.all(orders.map(async (o) => await getOrderData(o.id)))

    return res.send({ status: "ok", orders });

}));

router.get('/permissionlevel', auth("user"), handler(async (req, res, next, conn) => {

    var permission = (await conn.query(`SELECT permission FROM users WHERE id = ?`, [res.locals.userid]))[0].permission;

    return res.send({ status: "ok", permission });

}));

router.post('/changepassword', handler(async (req, res, next, conn) => {
    var {
        username,
        oldpass,
        newpass
    } = req.body;

    if (missingParams(res, { username, oldpass, newpass })) return;

    if (!newpass || !oldpass)
        return res.send({ status: "error", msg: "Password can't be empty!" });

    // Check if the username and old password are correct
    var oldPassHashDb = await conn.query("SELECT hash FROM users WHERE username = ?", [username]);
    if (oldPassHashDb.length == 0)
        return res.send({ status: "error", msg: "Authentication failed!" });

    if (!validatePass(res, oldpass, oldPassHashDb[0].hash)) return;

    // Store new password
    const sp = new SecurePass();
    var hash = (await sp.hashPassword(Buffer.from(newpass))).toString('base64');

    var updateRes = await conn.query("UPDATE users SET hash = ?, token = NULL WHERE username = ?", [hash, username]);
    if (updateRes.affectedRows == 0)
        return res.send({ status: "error", msg: "Couldn't update password!" });

    return res.send({ status: "ok" });

}));
router.post('/register', handler(async (req, res, next, conn) => {

    var {
        username,
        country,
        password,
    } = req.body;

    if (missingParams(res, { username, password, country })) return;

    // Check if username already exists
    var userExistsRes = await conn.query("SELECT id FROM users WHERE username = ?", [username]);
    if (userExistsRes.length != 0)
        return res.send({ status: 'error', msg: "User already exists!" });

    // Get the code of the given country
    var countryIdRes = await conn.query("SELECT id FROM `countrycodes` WHERE code = ?", [country]);
    if (countryIdRes.length === 0)
        return res.send({ status: "error", msg: "Unrecognised country code!" });
    var countryId = countryIdRes[0].id;

    // Hash the password
    const sp = new SecurePass();
    var hash = (await sp.hashPassword(Buffer.from(password))).toString('base64');

    // Insert to the database
    var insertRes = await conn.query("INSERT INTO users (username, hash, country) VALUES (?, ?, ?)", [username, hash, countryId]);
    if (insertRes.insertId == 0)
        return res.send({ status: "error", msg: "Couldn't insert data!" });

    return res.send({ status: "ok" });
}));

async function getOrderData(id : string){

	let orderedItems = await query(`
        SELECT colors.name as color, amount 
        FROM order_items INNER JOIN colors ON colors.id = order_items.color
        WHERE \`order\` = ?`, id)

	let assignedChips = await query(`
        SELECT colors.name as color, rfid 
        FROM assigned_chips INNER JOIN colors ON colors.id = assigned_chips.color
        WHERE \`orderid\` = ?`, id)
	
	const {user, date} = (await query(`
		SELECT users.username as user, date 
		FROM orders INNER JOIN users ON orders.user = users.id
		WHERE orders.id = ?`, id))[0];
	
	return new Order(id, [...orderedItems], [...assignedChips], date, user);
}


export default router;