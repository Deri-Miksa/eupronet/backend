import { Router } from "express";
import auth from "../middlewares/auth";
import handler from "../middlewares/handlerWrapper"
import { assemblyLineState } from "../model/handlers"

var router = Router();
router.get('/latest', auth("admin"), handler(async (req, res, next, conn) => {

    var limit = req.query.limit || req.body.limit || 10;
    var filter = req.query.endpoint || req.body.endpoint || "%";

    var activity = await conn.query(`
        SELECT 
            DATE_FORMAT(timestamp, "%Y-%m-%d %H:%i") as time,
            IFNULL(username, userid) as user,
            ${filter.includes("%") ? "endpoint" : ""}
        FROM activitylog
            LEFT JOIN users on users.id = userid
        WHERE endpoint LIKE ?
        ORDER BY timestamp DESC
        LIMIT ?`, [filter, limit]);

    return res.send({ status: "ok", activity });

}));

router.get('/logins', auth("admin"), handler(async (req, res, next, conn) => {

    var activity = await conn.query(`
        SELECT IFNULL(username, userid) as user,
        DATE_FORMAT(MAX(timestamp), "%Y-%m-%d %H:%i") as 'last_login_time'
        FROM activitylog
            LEFT JOIN users on users.id = userid
        WHERE endpoint = "/login"
        GROUP BY userid
        ORDER BY last_login_time DESC`);

    return res.send({ status: "ok", activity });

}));

router.get('/state', auth("dispatcher"), handler(async (req, res, next, conn) => {

    return res.send({ status: "ok", assemblyLineState });

}));

export default router;
