
import { Router } from "express";
import { SecurePass } from "argon2-pass";

import missingParams from "../middlewares/missingParams";
import handler from "../middlewares/handlerWrapper";
import auth from "../middlewares/auth";
import dispatch from "../nats/dispatcher";
require('dotenv').config();

var router = Router();
router.post('/truncate', auth("admin"), handler(async (req, res, next, conn) => {

    await conn.query("TRUNCATE TABLE queries");

    var prodStatsRes = await conn.query("CALL ProdStats()");
    // Sends the emptied table to all connected clients
    res.locals.io.emit("prodStats", prodStatsRes[0]);

    return res.send({ status: "ok", msg: "Successfully truncated queries table!" });

}));

router.get('/users', auth("admin"), handler(async (req, res, next, conn) => {

    var query = `SELECT id, username, country FROM users`;
    var rows = await conn.query(query);

    if (rows.length == 0) return res.send({ status: "error", msg: "There aren't any users in the database" });
    return res.send({ status: "ok", users: rows });

}));

// router.post('/users', auth("admin"), handler(async (req, res, next, conn) => {

//     var {
//         username,
//         country,
//         password,
//     } = req.body;

//     if (missingParams(res, { username, password, country })) return;

//     // Check if username already exists
//     var userExistsRes = await conn.query("SELECT id FROM users WHERE username = ?", [username]);
//     if (userExistsRes.length != 0)
//         return res.send({ status: 'error', msg: "User already exists!" });

//     // Get the code of the given country
//     var countryIdRes = await conn.query("SELECT id FROM `countrycodes` WHERE code = ?", [country]);
//     if (countryIdRes.length === 0)
//         return res.send({ status: "error", msg: "Unrecognised country code!" });
//     var countryId = countryIdRes[0].id;

//     // Hash the password
//     const sp = new SecurePass();
//     var hash = (await sp.hashPassword(Buffer.from(password))).toString('base64');

//     // Insert to the database
//     var insertRes = await conn.query("INSERT INTO users (username, hash, country) VALUES (?, ?, ?)", [username, hash, countryId]);
//     if (insertRes.insertId == 0)
//         return res.send({ status: "error", msg: "Couldn't insert data!" });

//     return res.send({ status: "ok" });

// }));

router.delete('/users', auth("admin"), handler(async (req, res, next, conn) => {
    var { username }: any = { ...req.query, ...req.body };

    if (missingParams(res, { username })) return;

    var deleteUserRes = await conn.query("DELETE FROM users WHERE username = ?", [username]);
    if (deleteUserRes.affectedRows == 0)
        return res.send({ status: "error", msg: "The requested user was not found in the database!" });

    return res.send({
        status: 'ok',
        msg: `Successfully deleted ${username} from the database`
    });

}));

router.post('/changepassword', auth("admin"), handler(async (req, res, next, conn) => {
    var {
        username,
        newpass
    } = req.body;

    if (missingParams(res, { username, newpass })) return;

    if (!newpass)
        return res.send({ status: "error", msg: "Password can't be empty!" });

    const sp = new SecurePass();
    var hash = (await sp.hashPassword(Buffer.from(newpass))).toString('base64');

    var updateRes = await conn.query("UPDATE users SET hash = ?, token = NULL WHERE username = ?", [hash, username]);
    if (updateRes.affectedRows == 0)
        return res.send({ status: "error", msg: "This username doesn't exist!" });

    return res.send({ status: "ok" });

}));


export default router;