import { Msg, StringCodec } from "nats";
import dispatch from "../nats/dispatcher";
import { ColorReading, Color, Order, EPNMessage, RFIDReading, QualityControl, OrderID, PrintJob, Chip, OrderItem, colors } from "./types";
import { onMsg } from "../nats/listeners";
import { Server, Socket } from "socket.io";
import { io } from "../app";
import { query } from "../bin/db";
import _ from "lodash";

import pool from "../bin/db";

// var colorReadings: ColorReading[] = [];
// var RFIDColorPairs: { [key: string]: Color } = {};
// var activeOrders: Order[] = [];
// var finishedOrders: Order[] = [];

export interface FactoryState {
	colorReadings: ColorReading[]
	RFIDColorPairs: { [key: string]: Color }
	activeOrders: Order[]
	finishedOrders: Order[]
}
export let assemblyLineState: FactoryState = {
	colorReadings: [],
	RFIDColorPairs: {},
	activeOrders: [],
	finishedOrders: []
}

var sc = StringCodec();

const statehandlers: { channel: string, handlers: [number, (msg: EPNMessage, state: FactoryState, carrier: Msg) => void][] }[] = [
	{
		channel: "backend", handlers: [
			[601, // Queue order
				({ payload }, { activeOrders }) => {
					let order = Order.from(payload as Order);

					let items = order.orderedItems.map(({ color, amount }) =>
						({ color, amount: amount - order.countAssigned(color) }))

					// console.log(items)

					dispatch("printer.job", {
						code: 101,
						label: "New print job",
						payload: {
							orderid: order.id,
							items
						} as PrintJob
					});

					let { id, orderedItems, assignedChips } = order
					activeOrders.push(new Order(id, orderedItems, assignedChips));
				}]
		]
	},
	{
		channel: "printer.telemetry", handlers: [
			[104, //Assembly finished
				(msg) => {
					dispatch("colorarm.job", { code: 201, label: "Chip ready" })
				}]]
	},
	{
		channel: "colorarm.telemetry", handlers: [
			[203, //Color reading
				(msg, { colorReadings }) => {
					// console.log(msg)s
					colorReadings.push(msg.payload as ColorReading);
				}],
			[204, //Swing complete
				(msg) => {
					dispatch("rfidarm.job", { code: 301, label: "Chip ready" })
				}]]
	},
	{
		channel: "rfidarm.telemetry", handlers: [
			[303, //RFID reading
				(msg, { colorReadings, RFIDColorPairs }) => {
					var color = colorReadings.shift()?.color;
					const { rfid } = msg.payload as RFIDReading;
					if (!rfid) return console.error("303 - Invalid payload - RFID missing");
					if (!color) return console.error("303 - No color reading stored");

					RFIDColorPairs[rfid] = color;
				}],
			[304, //Swing done
				(msg) => {
					dispatch("chipsorter.job", { code: 401 }) //Chip ready
				}]]
	},
	{
		channel: "chipsorter.*", handlers: [
			[402, //Is chip part of an order?
				(msg, { RFIDColorPairs, activeOrders }, carrier) => {
					const { rfid } = msg.payload as RFIDReading
					if (!rfid) return console.error("402 - Invalid payload - RFID missing");

					let color = RFIDColorPairs[rfid];
					if (!color) return console.error(`402 - No color paired with RFID: ${rfid}`);

					const ordered = activeOrders.some(o => o.needsChip(color));
					// console.log(ordered);


					let epnm: EPNMessage = {
						code: 412,
						payload: {
							rfid,
							ordered
						}
					}
					if (carrier.reply) {
						carrier.respond(sc.encode(JSON.stringify(epnm)));
					} else {
						dispatch("chipsorter.job", epnm);
					}
				}],
			[403, //Passed
				(msg, { RFIDColorPairs, activeOrders }) => {
					const { rfid, passed } = msg.payload as QualityControl
					if (!rfid) return console.error("403 - Invalid payload - RFID missing");
					let color = RFIDColorPairs[rfid];
					if (!color) return console.error(`403 - No color paired with RFID: ${rfid}`);
					if (!passed) {
						//This chip was discarded. If there is an order that needs it, we'll reprint it.
						if (activeOrders.some(o => o.needsChip(color))) {
							dispatch("printer.job", {
								code: 101,
								label: "Reprinting failed chip",
								payload: {
									items: [{ color, amount: 1 }]
								} as PrintJob
							});
						}

						delete RFIDColorPairs[rfid];
						return console.log(`Chip with RFID "${rfid}" failed quality control.`);
					}

					const chip: Chip = { rfid, color: RFIDColorPairs[rfid] };
					const order = activeOrders.find(o => o.needsChip(chip.color))

					if (!order) return console.error(`There are no unfinished orders with ${chip.color} chips missing`);

					dispatch("packaging.job", { code: 501, label: "Packaging instruction", payload: { rfid, orderid: order.id } })

					order.assignedChips.push(chip);
					assignChipInDB(chip, order)
					io.emit("chip-assigned", order)
				}]]
	},
	{
		channel: "packaging.telemetry", handlers: [
			[502, // Magazine ready
				(msg, { activeOrders }) => {
					dispatch("chipsorter.job", { code: 404, label: "Start conveyor" })

					const id = (msg.payload as OrderID).orderid;
					const order = activeOrders.find(o => o.id == id);

					if (!order) return console.error(`502 - order#${id} not found in active orders`)

					//Check if order is done
					if (order.assembled) {
						dispatch("packaging.job", { code: 503, label: "Order assembled", payload: { orderid: order.id } as OrderID })
					}
				}],
			[504, // Delivered
				(msg, { activeOrders, finishedOrders, RFIDColorPairs }) => {
					const id = (msg.payload as OrderID).orderid;
					const order = activeOrders.find(o => o.id == id);

					if (!order)
						return console.error(`502 - There is no active order with id ${id}`);

					finishedOrders.push(order) //move to history

					// Free up pairings
					for (const rfid in RFIDColorPairs) {
						if (order.assignedChips.some(chip => chip.rfid == rfid))
							delete RFIDColorPairs[rfid];
					}
					_.remove(activeOrders, o => o.id == order.id)
				}]]
	}
]

export function queueOrder(order: Order) {
	dispatch("backend", {
		code: 601,
		label: "New order",
		payload: order
	}, true);
}

export default async function setup() {
	for (const s of statehandlers) {
		await onMsg(s.channel, (msg, carrier) => {
			const handler = s.handlers.find(([code]) => code == msg.code)
			if (!handler) return console.error(`No applicable handler found for message: \n ${JSON.stringify(msg)}`)
			const [_, callback] = handler;
			callback(msg, assemblyLineState, carrier);

			// Send state updates after every event
			sendStateUpdate();
		})
	}

	sendStateUpdate(); // For those who immediately connect after the server restarts
	io.on("connection", (socket: Socket) => {
		// Send state when the client connects
		socket.emit("telemetry", assemblyLineState);
	})
}

async function assignChipInDB({ color, rfid }: Chip, order: Order) {
	let res = await query("SELECT * FROM orders WHERE id = ?", order.id)
	if (res.length == 0) return console.log(`Couldn't assign chip: order#'${order.id}' was not found in database.`);

	let colorid: number = (await query("SELECT id FROM colors WHERE upper(?) = name", color))[0].id;

	let exists = await query("SELECT id FROM assigned_chips WHERE rfid = ? AND color = ? AND orderid = ?", rfid, colorid, order.id);
	if (exists.length > 0) return console.log(`Duplicate chip assignment: ${rfid} - ${color}`)

	res = await query("INSERT INTO `assigned_chips` (`orderid`, `rfid`, `color`) VALUES (?, ?, ?)", order.id, rfid, colorid)

	if (res.affectedRows == 0)
		console.error(res)

}


export function checkColorReadings(colorReadings: ColorReading[] | undefined) {
	if (!colorReadings || colorReadings.every(x => colors.includes(x.color)))
		return "";

	return "colorReadings";
}
export function checkRFIDColorPairs(RFIDColorPairs: { [key: string]: Color }) {
	if (!RFIDColorPairs || Object.values(RFIDColorPairs).every(x => colors.includes(x)))
		return "";

	return "RFIDColorPairs";
}

export function checkActiveOrders(activeOrders: Order[]) {
	if (!activeOrders || activeOrders.every(x => x.id))
		return "";

	return "activeOrders";
}

export function checkFinishedOrders(finishedOrders: Order[]) {
	if (!finishedOrders || finishedOrders.every(x => x.id))
		return "";

	return "finishedOrders";
}

export function sendStateUpdate() {
	io.emit("telemetry", assemblyLineState);
}