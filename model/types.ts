import _ from "lodash";

export const countryCodes = ["BE", "CZ", "GE", "HU", "IS", "NO"] as const;
export type CountryCode = typeof countryCodes[number];

export const colors = ["RED", "WHITE", "YELLOW"] as const
export type Color = typeof colors[number];

export interface EPNMessage {
	//country: countryCode
	code: number,
	label?: string,
	payload?: PayloadType
}

export class NonePayload {
	public constructor() {}
}

export class PrintJob {
	public constructor(
		public orderid: string = "",
		public items: OrderItem[] = []
	) {}
}

export class ColorReading {
	public constructor (
		public color: Color = "RED"
	) {}
}
export class RFIDReading {
	public constructor (
		public rfid: string = ""
	) {}
}

export class PartOfOrderResponse {
	public constructor (
		public rfid: string = "",
		public ordered: boolean = false
	) {}
}

export class QualityControl {
	readonly reason?: string = "";
	public constructor (
		public rfid: string = "",
		public passed: boolean = false,
		reason: string = ""
	) { this.reason = reason }
}

export class PackagingInstruction {
	public constructor (
		public rfid: string = "",
		public orderid: string = ""
	) {}
}

export class OrderID {
	public constructor (
		public orderid: string = ""
	) {}
}

export const payloadTypes = [
	new NonePayload(),
	new PrintJob(),
	new ColorReading(),
	new RFIDReading(),
	new PartOfOrderResponse(),
	new QualityControl(),
	new PackagingInstruction(),
	new OrderID()
] as const;
export type PayloadType = typeof payloadTypes[number];

export const payloadClassTypes = [
	NonePayload,
	PrintJob,
	ColorReading,
	RFIDReading,
	PartOfOrderResponse,
	QualityControl,
	PackagingInstruction,
	OrderID
] as const;
export type PayloadClassType = typeof payloadClassTypes[number];

export class CodeRecord {
	readonly payloadType: PayloadClassType = NonePayload
	public constructor(
		readonly code: number,
		readonly label: string,
		readonly direction: DDR,
		payloadType: PayloadClassType = NonePayload
	) { this.payloadType = payloadType }
}

export enum DDR {
	FROM_SERVER = 0,
	TO_SERVER = 1
}

const [ fromServer, toServer ] = [ DDR.FROM_SERVER, DDR.TO_SERVER ];

export class Workstation {
	constructor(
		readonly label: string,
		readonly country: CountryCode,
		readonly codetable: CodeRecord[],
	) {}

	public get job() {
		return this.label + ".job"
	}
	public get telemetry() {
		return this.label + ".telemetry"
	}
}

export const workstations = {
	printer: new Workstation("printer", "CZ", [
		new CodeRecord(101, "Print job", fromServer, PrintJob),
		new CodeRecord(102, "Print in progress", toServer),
		new CodeRecord(103, "Print finished", toServer),
		new CodeRecord(104, "Assembly finished", toServer)
	]),
	colorarm: new Workstation("colorarm", "IS", [
		new CodeRecord(201, "Chip ready", fromServer), 
		new CodeRecord(202, "Swing start", toServer),
		new CodeRecord(203, "Color reading", toServer, ColorReading),
		new CodeRecord(204, "Swing complete", toServer)
	]),
	rfidarm: new Workstation("rfidarm", "NO", [
		new CodeRecord(301, "Chip ready", fromServer), 
		new CodeRecord(302, "Swing start", toServer), 
		new CodeRecord(303, "RFID reading", toServer,RFIDReading),
		new CodeRecord(304, "Swing complete", toServer),
	]),
	chipsorter: new Workstation("chipsorter", "GE", [
		new CodeRecord(401, "Chip ready", fromServer),
		new CodeRecord(402, "Is chip part of an order? (request)",	toServer, RFIDReading),
		new CodeRecord(412, "Is chip part of an order? (response)", fromServer, PartOfOrderResponse),
		new CodeRecord(403, "Quality control", toServer, QualityControl),
		new CodeRecord(404, "Start conveyor", fromServer)
	]),
	packaging: new Workstation("packaging", "BE", [
		new CodeRecord(501,"Chip ready", fromServer, PackagingInstruction),
		new CodeRecord(502,"Magazine ready", toServer, OrderID),
		new CodeRecord(503,"Order assembled", fromServer, OrderID),
		new CodeRecord(504,"Delivered", toServer, OrderID)
	])
} as const;

export interface OrderItem {
	color: Color;
	amount: number;
}

export interface Chip {
	rfid: string,
	color: Color
}

export class Order {
	constructor(
		readonly id: string,
		readonly orderedItems: OrderItem[],
		public assignedChips: Chip[] = [],
		readonly date?: Date,
		readonly userid?: number,
	) {}

	public static from(order : Order){
		return new Order(order.id, order.orderedItems, order.assignedChips ?? [], order.date, order.userid);
	}

	public countOrdered(color: Color): number {
		return _.sum(this.orderedItems.filter(x => color == x.color).map(x => x.amount));
	}

	public countAssigned(color: Color): number {
		return this.assignedChips.filter(x => x.color == color).length;
	}

	public needsChip(color: Color): boolean {
		// console.log(color, this.countOrdered(color), this.countAssigned(color));
		
		return this.countOrdered(color) > this.countAssigned(color);
	}

	public get assembled() {
		return this.orderedItems.every(x => this.countOrdered(x.color) == this.countAssigned(x.color))
	}
}