import { getConnection } from "./connect";
import { Msg, NatsConnection, StringCodec, Subscription } from "nats";
import { EPNMessage } from "../model/types";


var sc = StringCodec();
var nc: NatsConnection | undefined;

export async function onMsg (subject: string, callback: (msg: EPNMessage, carrier: Msg) => void) {
	nc = await getConnection();
	if (!nc) return console.error("Failed getting a NATS connection");

	var sub: Subscription = nc.subscribe(subject);
	(async (sub: Subscription) => {
		console.log(`listening for ${sub.getSubject()} requests...`);
		for await (const m of sub) {
			try {
				let decoded = sc.decode(m.data);
				console.log(decoded);
				callback(JSON.parse(decoded), m);
			} catch (e) {
				console.log(`Error: ${e}`);
			}
		}
		console.log(`subscription ${sub.getSubject()} drained.`);
	})(sub).catch(err => {
		console.error(`Couldn't subscribe to ${subject}. ${err}`);
	});
}