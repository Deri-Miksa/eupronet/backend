import { connect, NatsConnection } from "nats";

async function natsConnect() {
	try {
		if (!process.env.NATS_server) return;
		var nc = await connect({
			servers: [process.env.NATS_server],
			// port: Number(process.env.NATS_port || 4222),
			maxReconnectAttempts: -1,
			user: process.env.NATS_user,
			pass: process.env.NATS_password
		});
		if (process.env.NODE_ENV === "development")
			console.log(`Successfully connected to ${nc.getServer()}!`);
		return nc;
	} catch (err) {
		console.log(`Failed connecting to nats server: ${err}`);
		return undefined;
	}
};

var nc: NatsConnection | undefined;
export async function getConnection() {
	if (!nc || nc.isClosed())
		nc = await natsConnect();

	return nc;
}