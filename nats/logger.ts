import pool from "../bin/db";
import { onMsg } from "./listeners";

import time from "strftime";

export let loggingEnabled = process.env.NODE_ENV == "production";

export function setLoggingEnabled(enabled: boolean) {
	loggingEnabled = enabled
}

onMsg(">", async ({code, payload}, carrier) => {

	if (!loggingEnabled) return;

	let conn;
	try {
		conn = await pool.getConnection();
		await conn.query(`INSERT INTO natslog (subject, code, payload, timestamp) VALUES ( ?, ?, ?, ?)`, 
			[carrier.subject, code, JSON.stringify(payload ?? {}), time("%Y-%m-%d %H:%M:%S")]);

	}catch(err){
		console.error(err);
	} finally {
		if (conn) return conn.release();
	}
})

