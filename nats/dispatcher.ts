import { getConnection } from "./connect";
import { StringCodec } from "nats";
import { EPNMessage } from "../model/types"
var sc = StringCodec();

export let dispatchEnabled = process.env.NATS_default_state == "enabled" || process.env.NODE_ENV == "production";

export default async function dispatch(subject: string, message: EPNMessage, force = false) {
	if(!force && !dispatchEnabled) return;

	var nc = await getConnection();
	if (!nc) return new Error("Couldn't establish connection with NATS!");
	nc.publish(subject, sc.encode(JSON.stringify(message)));
}

export function setDispatchEnabled(enabled : boolean){
	dispatchEnabled = enabled;
}
