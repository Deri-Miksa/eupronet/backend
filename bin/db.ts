import mariadb from "mariadb";
import dotenv from "dotenv";
dotenv.config()

// create a new connection pool
const pool = mariadb.createPool({
	host: process.env.host,
	user: process.env.user,
	password: process.env.password,
	database: process.env.database
});


// expose the ability to create new connections
export default {
	getConnection: async function () {
		return await pool.getConnection();
	}
}

export async function query(sql: string, ...params: any) {
	let conn;
	try {
		conn = await pool.getConnection();

		return await conn.query(sql, params);
	} catch (err) {
		console.error(err);
	} finally {
		if (conn) conn.release();
	}
}