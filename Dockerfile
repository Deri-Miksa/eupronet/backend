FROM node:erbium
ENV TZ=Europe/Budapest

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm i

COPY . .
RUN npx tsc
EXPOSE 3000
CMD [ "node", "./dist/bin/www" ]
