module.exports = {
    send: {
        error: (res, msg, status = 400) => {
            res.status(status).send({ status: "error", msg: msg });
        },

        ok: (res, obj) => {
            res.status(200).send({ status: "ok", ...obj })
        }
    }
}