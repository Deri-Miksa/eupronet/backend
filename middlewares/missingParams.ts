import { Response } from "express";

export default function missingParams(res: Response, ...groups: {[x: string]: any}[]) {
    // All the groups which have missing values
    var undefs = groups.filter(obj => Object.keys(obj).filter(key => obj[key] === undefined).length != 0);
    // All the groups which have any values defined
    var defs = groups.filter(obj => Object.keys(obj).filter(key => obj[key] !== undefined).length != 0);

    if (undefs.length != groups.length) { // There is an object which has all values defined
        return false;
    } else {
        // If no values are defined at all then take the undefs array
        var incomplete = defs.length > 0 ? defs : undefs;
        res.send({
            status: "error",
            msg: `Missing parameters: ${incomplete.map(x => Object.keys(x).filter(key => x[key] === undefined) // Gets the undefined elements
                .map(y => `'${y}'`).join(', ')) // Wrap in quotes
                .join(' or ')}` // Join the groups
        });
        return true;
    }
}