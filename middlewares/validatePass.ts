import { Response } from "express";
import { SecurePass } from "argon2-pass";

export default function (res: Response, password: string, hash: string) {
    const sp = new SecurePass();
    const passComp = sp.verifyHashSync(Buffer.from(password), Buffer.from(hash, 'base64'));

    if (SecurePass.isValid(passComp)) {
        return true;
    } else if (SecurePass.isInvalidOrUnrecognized(passComp) || SecurePass.isInvalid(passComp)) {
        res.send({
            status: 'error',
            msg: "Authentication failed!"
        });
    } else if (SecurePass.isValidNeedsRehash(passComp)) {
        res.send({
            status: 'error',
            msg: "Please contact the sysadmin team"
        });
    }

    return false;
}