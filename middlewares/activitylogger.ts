import { NextFunction, Request, Response } from "express";
import { PoolConnection } from "mariadb";
import time from "strftime";
import handler from "./handlerWrapper";

export default handler(async (req: Request, res: Response, next: NextFunction, conn: PoolConnection) => {
	res.on("finish", async () => {
		if (res.locals.userid === undefined) return;
		await conn.query(`INSERT INTO activitylog (userid, endpoint, timestamp) VALUES ( ?, ?, ?)`, 
			[res.locals.userid, req.path, time("%Y-%m-%d %H:%M:%S")]);
	});
	next();
});

// module.exports = function (req, res, next) {

//     res.on('finish', async () => {
//         if (res.locals.userid === undefined) {
//             return;
//         }

//         let conn;
//         try {

//             conn = await pool.getConnection();
//             await conn.query(`INSERT INTO activitylog (userid, endpoint, timestamp) 
//             VALUES ( ?, ?, ?)`, [res.locals.userid, req.path, time("%Y-%m-%d %H:%M:%S")]);

//         } catch (err) {
//             next(err);
//         } finally {
//             if (conn)
//                 return conn.release();

//         }
//     });
//     next();

// }
