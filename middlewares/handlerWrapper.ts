import { NextFunction, Request, Response } from "express";
import { PoolConnection } from "mariadb";

import pool from "../bin/db";

export default (callback: (req: Request, res: Response, next: NextFunction, conn: PoolConnection) => Promise<any>) => {
	return async (req: Request, res: Response, next: NextFunction): Promise<any> => {
		let conn;
		try {
            conn = await pool.getConnection();
            await callback(req, res, next, conn);
        } catch (err) {
            next(err);
        } finally {
            if (conn) return conn.release();
        }
	}
}