import { NextFunction, Request, Response } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
require('dotenv').config();
import pool from "../bin/db";

export default function (privilege: "user" | "dispatcher" | "admin") {
    return async function (req: Request, res: Response, next: NextFunction) {
        let token: string = req.headers['x-access-token'] || req.headers['authorization'] || req.body.token;

        if (!token)
            return res.send({
                status: "error",
                msg: "Authentication failed: missing token!"
            })

        if (token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length);
        }

        // On the admin privilege check if the the token matches the admin token
        if (privilege == "admin") {
            if (token != process.env.adminToken) {
                return res.send({
                    status: 'error',
                    msg: "Authentication failed: invalid admin token!"
                });
            }
        }
        // On the user privilege check the token in the database
        else if (privilege == "user" || privilege == "dispatcher") {
            if (token == process.env.adminToken) {
                next();
                return;
            }
            try {
                var decoded = jwt.verify(token, (process.env.JWTSecret || process.env.adminToken as string)) as JwtPayload;
                res.locals.userid = decoded.uid;

                if (privilege == "dispatcher") {
                    let conn = await pool.getConnection();
                    let permissionlevel = (await conn.query("SELECT permission FROM users WHERE id = ?", [res.locals.userid]))[0].permission;
                    if (conn) conn.release();
                    if (permissionlevel < 7) {
                        return res.send({
                            status: "error",
                            msg: "Authentication failed: you do not have permission to access the dispatcher!"
                        });
                    }
                }
            }
            catch (err) {
                return res.send({
                    status: "error",
                    msg: "Authentication failed: invalid token!"
                });
            }
        }
        else {
            next("Privilege unknown!");
        }
        next();
    }
}