import axios from "axios";
import assert from "assert";
require('dotenv').config();

var testUsername = "test";
var testPassword = "test";
var testToken = "";

describe('Admin endpoints', () => {
    const instance = axios.create({
        baseURL: `${process.env.domain}/admin`,
        timeout: 2000,
        headers: { Authorization: process.env.adminToken }
    });


    it('Create user', async () => {
        var res = await instance.post(`/users`, {
            username: testUsername,
            password: testPassword,
            country: "hu"
        });
        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });

    it('Get users', async () => {
        var res = await instance.get(`/users`);
        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });

    it('Change password', async () => {
        var res = await instance.post(`/changepassword`, {
            username: testUsername,
            newpass: "test2"
        });
        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });

    it('Delete user', async () => {
        var res = await instance.delete(`/users?username=${testUsername}`);
        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });
});

describe('User endpoints', () => {
    const instance = axios.create({
        baseURL: `${process.env.domain}`,
        timeout: 2000,
    });

    before(async () => {
        var res = await instance.post(`/admin/users`, {
            username: testUsername,
            password: testPassword,
            country: "hu"
        }, { headers: { Authorization: process.env.adminToken } });

        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });

    after(async () => {
        var res = await instance.delete(`/admin/users?username=${testUsername}`,
            { headers: { Authorization: process.env.adminToken } });

        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });

    it('Change password', async () => {
        var newpass = "test2";
        var res = await instance.post(`/changepassword`, {
            username: testUsername,
            oldpass: testPassword,
            newpass: newpass
        });
        assert.strictEqual(res.data.status, "ok", res.data.msg);

        testPassword = newpass;
    });

    it('Login', async () => {
        var res = await instance.post(`/login`, {
            username: testUsername,
            password: testPassword,
        });
        assert.strictEqual(res.data.status, "ok", res.data.msg);
        assert.ok(res.data.token, "Empty token");

        testToken = res.data.token;
    });

    it('Insert print', async () => {
        var res = await instance.post(`/insert/print`, {
            token: testToken,
            state: 1,
            color: 1
        });
        assert.strictEqual(res.data.status, "ok", res.data.msg);
    });

    it('Color from RFID tag', async () => {
        var res = await instance.get(`/colorfromrfid?rfid=1`, {
            headers: { Authorization: testToken }
        });
        assert.strictEqual(res.data.status, "ok", res.data.msg);
        assert.strictEqual(res.data.color, "WHITE");
    });
});

describe('Telemetry endpoints', () => {
    const instance = axios.create({
        baseURL: `${process.env.domain}/telemetry`,
        timeout: 2000,
        headers: { Authorization: process.env.adminToken }
    });

    it('Latest', async () => {
        var res = await instance.get(`/latest`);
        assert.strictEqual(res.data.status, "ok", res.data.msg);
        assert.ok(res.data.activity);
    });

    it('Logins', async () => {
        var res = await instance.get(`/logins`);
        assert.strictEqual(res.data.status, "ok", res.data.msg);
        assert.ok(res.data.activity);
    });
});
