import { Server, Socket } from "socket.io";
import express, { NextFunction, Request, Response } from "express";
import createError from "http-errors";
import http from "http";
import cookieParser from "cookie-parser";
import logger from "morgan";
import cors, { CorsOptions } from "cors";
import path from "path";

import pool from "./bin/db";
import userRouter from "./routes/user";
import adminRouter from "./routes/admin";
import telemetry from "./routes/telemetry";
import natsRouter from "./routes/nats";

import "./nats/listeners";
import setupStateHandlers from "./model/handlers"

var app = express();
var server = http.createServer(app);

var whitelist = ['https://eupronet.hu', 'https://dev.eupronet.hu', 'https://dev.derimiksa.hu', 'https://demo.derimiksa.hu']
var corsOptions: CorsOptions = process.env.NODE_ENV !== "development" ? {
	origin: function (origin, callback) {
		if (!origin || whitelist.indexOf(origin) !== -1)
			callback(null, true)
		else
			callback(new Error('Not allowed by CORS'));
	},
	credentials: true
} : {};

app.use(cors(corsOptions));

export var io = new Server(server, {
	cors: corsOptions
});

io.on('connection', async (socket: Socket) => {
	// socket.emit('telemetry', { status: "ok", msg: "Connection successful!" });
	let conn;
	try {
		conn = await pool.getConnection();
		var prodStatsSQL = "CALL ProdStats()";

		var rows = await conn.query(prodStatsSQL);

		socket.emit("prodStats", rows[0]);
	}
	catch (err) {
		throw err;
	}
	finally {
		if (conn) return conn.release();
	}
});

setupStateHandlers();

app.set('view engine', 'pug');

app.use(function (req: Request, res: Response, next: NextFunction) {
	res.locals["io"] = io;
	next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', userRouter);
app.use('/admin', adminRouter);
app.use('/telemetry', telemetry);
app.use('/nats', natsRouter);

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: NextFunction) {
	next(createError(404));
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = {};
	if (req.app.get('env') === 'development') {
		console.log(err);
		res.locals.error = err;
	}
	// render the error page
	res.status(err.status || 500);
	res.send({ status: "error", msg: err.message });
});

// Export instantiated server back to www
export { app, server };
